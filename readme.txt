MFP local Server setup

1. I downloaded MobileFirst Development kit 
2. After installation to start mfpserver in cmd run run.cmd


MFP Analytics

1. create ionic project - ionic start mfpionicwebapp blank
2. install ibm web sdk - npm i ibm-mfp-web-sdk --save
3. to check if every plugins is installed - npm list --depth=0
	- in my case there was an error on jasmine-core@3.4.0 
	- I install the version 3.5.0 - npm install jasmine-core@3.5.0
4. Register app on local mfpserver
	- mfpdev app register
	- applicationid - com.mfp.ionicwebapp
5. In the src folder I created customer-typings.d.ts file and pasted
	- /// <reference path="../node_modules/ibm-mfp-web-sdk/lib/typings/ibmmfpf.d.ts"/>

6. I added this javascript files in the scripts angular.json file
	      "node_modules/ibm-mfp-web-sdk/ibmmfpf.js",
              "node_modules/ibm-mfp-web-sdk/lib/analytics/ibmmfpfanalytics.js",
              "node_modules/sjcl/sjcl.js",
              "node_modules/jssha/src/sha.js"

7. I added code in the home page
	Sample Log:
    		ibmmfpfanalytics.init('device-uuid-36', 'com.mfp.ionicwebapp', 'mfp');
	    	ibmmfpfanalytics.setUserContext('John Doe');
    		ibmmfpfanalytics.logger.info('','Ionic Web App');
    		ibmmfpfanalytics.send();

8. In the root folder i have created proxy.conf.json which indicates my mfp server
	    "/mfp": {
	       "target": "http://192.168.0.100:9080",
	       "secure": false
	    }

9. to test - ng serve --proxy-config proxy.conf.json --open





