import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  public message:any;
  constructor() {

  }

  sendLog(){
    alert("Send");
    ibmmfpfanalytics.init('device-uuid-340', 'com.mfp.ionicwebapp', 'mfp');
    ibmmfpfanalytics.setUserContext('John Doe');
    ibmmfpfanalytics.logger.info('','Ionic Web App Log');
    ibmmfpfanalytics.send();
  }

}
